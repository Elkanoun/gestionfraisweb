package smartsofts.applicatif.gestionfrais.entities;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor @AllArgsConstructor @Data 
public class Credit implements Serializable {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date date;
	private Double montant;
	private String source;
	private Date datePayement;
	private Boolean isPayed;
	
	@ManyToOne
	@JoinColumn(name = "idComptabilite")
	private Comptabilite comptabilite;
	
	

}
