package smartsofts.applicatif.gestionfrais.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor @AllArgsConstructor @Data 
public class Comptabilite implements Serializable {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date dateComptabilite;
	private Double totalDebits;
	private Double totalCredits;
	private Double totalRevenus;
	private Boolean isGain;
	
	@OneToMany(mappedBy = "comptabilite", fetch = FetchType.LAZY)
	private List<Debit> debits;
	
	@OneToMany(mappedBy = "comptabilite", fetch = FetchType.LAZY)
	private List<Credit> credits;
	

}
